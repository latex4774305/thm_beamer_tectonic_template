# THM_tectonic_template
The main goal of this [LaTex](https://www.latex-project.org/) repository is to provide a general approach for a 
[Latex Beamer](https://ctan.org/pkg/beamer) template that follows the guidelines of the 
[University of Applied Sciences Mittelhessen THM](https://www.thm.de/site/en/). The main reason for creating this 
template with the help of the [Tectonic Framework](https://tectonic-typesetting.github.io/book/latest/) was to 
make the whole template as clean as possible with a sophisticated compile and project management structure. The 
template includes all official logos of the THM, so you can adapt the template to the faculty you're currently 
studying with.
The main files are:

- Tectonic.toml - Configuration file for Tectonic, which will compile the LaTeX files to PDF
- `/src/_preamble.tex` - Sets up the document class, packages, theme, author etc.
- `/src/_postamble.tex` - Closes the document
- `/src/index.tex` - Main content file that just contains "Hello World" for this example
- `/src/beamerthemeTHM.sty` Contains the theme definition, including colors, logos, templates for sections, blocks etc.

It defines a custom Beamer theme called "THM" with colors, logos and styling specific to that university. The logos and theme elements are loaded from a /themeRessources folder.
There are also some LaTeX formatting tweaks like section numbering, table of contents style, and itemize bullets.
So in summary, it is a Beamer presentation theme for the THM university, packaged up in a way that it can be compiled to PDF using Tectonic.
## How to install tectonic
You'll find installation instructions for tectonic right here on the 
[tectonic website](https://tectonic-typesetting.github.io/en-us/).
## How to use
Classic Tectonic projects offer a very streamlined project structure. The main code files are located in the 
`src/` folder. Meanwhile, if you go to the root of the project and type `tectonic -X build`, you'll find the 
output in the `build/default/` directory. The beamer template for this template can be found with the main 
source files in the `src/` directory.
### Structure inside the `src/` directory
Here you'll find four main files.
#### `_preamble.tex` 
should contain all of your (La)TeX initialization boilerplate, up to and including the LaTeX `\begin{document}` command.
#### `_postamble.tex` 
should contain all of your cleanup code, starting with the LaTeX `\end{document}` command. There will almost never need 
to be any other content in this file.
#### `index.tex` 
contains all of your actual document content, without any of the annoying boilerplate. In this case, it contains an example 
presentation you can compile and play with.
#### `beamerthemeTHM.sty`
The `/src/beamerthemeTHM.sty` file defines the custom Beamer theme for THM presentations. Here are some key aspects:

Defines THM specific colors like THMGray, THMGreen that are used for different elements

Imports university logo images from `/themeRessources` folder and assigns them to macros like `\thmCGMNI`

Sets beamer colors using the custom colors, to style title, subtitles, text etc.

Defines custom templates for sections, subsections in table of contents

Sets formatting for frame titles, blocks, itemize bullets

Defines conditionals and macros to control logo display in different slides

Imports faculty/department specific logos like `\thmCFM`, `\thmCGBAU` and assigns them to macros

Sets size of margins and other dimensions

Overall, it has all the code to define visual styling, colors, fonts, templates that make up the THM theme.

So this file contains the core theme definition and customizations that adapt the default Beamer look to match THM branding and style guide.
